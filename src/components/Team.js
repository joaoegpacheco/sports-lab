import React from 'react';

function Team (props) {

let shotPercentageDiv 

  if (props.stats.shots) {
    const shotPercentage = Math.round((props.stats.score / props.stats.shots) * 100)
    shotPercentageDiv = (
      <div>
        <strong>Aproveitamento: {shotPercentage}%</strong>
      </div>
    )
  }



  return (
      <div className="Team">

          <img src={props.logo} alt={props.name} width={50}  />
        <h2>{props.name}</h2>

        <div>
          <strong>Chutes Realizados:</strong> {props.stats.shots}
        </div>

        <div>
          <strong>Pontos:</strong> {props.stats.score}
        </div>

        {shotPercentageDiv}

        <button onClick={props.shotHandler}>Chutar !!</button>


      </div>
  );
}

export default Team;