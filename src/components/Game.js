import React from 'react';
import Team from './Team'
import shotSound from '../song/caught.wav';
import scoreSound from '../song/applause.wav';
import brasil from '../img/brasil.png';
import alemanha from '../img/alemanha.png';

class Game extends React.Component {

state = {
  resetCount: 0,
  homeTeamStats: {
    shots: 0,
    score: 0
  },
  visitingTeamStats: {
    shots: 0,
    score: 0
  }
}
//score arrumar por equipe 
shotHandler = (team) => {
  const teamStatsKey = `${team}TeamStats`
  let score = this.state[teamStatsKey].score
  let songShots = new Audio(shotSound)
  let songScore = new Audio(scoreSound)
  
  songShots.play()
  if (Math.random() > 0.4) {
    score += 1
    setTimeout(() => {
      songScore.play()

    }, 100)
  }
  this.setState((state, props) => ({
    [teamStatsKey] : {
    shots: state[teamStatsKey].shots + 1,
    score
    }
  }))
}

resetGame = () => {
  this.setState((state, props) => ({
    resetCount: state.resetCount + 1,
    homeTeamStats: {
      shots: 0,
      score: 0
    },
    visitingTeamStats: {
      shots: 0,
      score: 0
    }
  }))
}

  render() {

    return (
      <div className="Game">
        <h1>Seja bem-vindo</h1>
        <div className="game-state">
          <Team 
            name = { this.props.homeTeam.name } 
            logo = { brasil }
            stats = { this.state.homeTeamStats }
            shotHandler={() => this.shotHandler("home")}
          />
        
        <div className="vs">
          <h5>vs</h5>
        </div>

          <Team 
            name = { this.props.visitingTeam.name } 
            logo = { alemanha }
            stats = { this.state.visitingTeamStats }
            shotHandler={() => this.shotHandler("visiting")}
          />

          </div>
        <br />
          <div>
            <strong>Quantas vezes reiniciou:</strong> {this.state.resetCount}
            <button onClick = { this.resetGame }>Reiniciar Jogo</button>  
          </div>

        </div>
    );
  }
}

export default Game;
