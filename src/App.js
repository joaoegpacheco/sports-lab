import React from 'react';
import './App.css'
import Game from './components/Game'

function App (props) {

const home = {
    name: "Brasil"
}

const visiting = {
    name: "Alemanha"
}
    return (
        <div className="App">
            <Game 
                homeTeam = { home }
                visitingTeam = { visiting }
            />
        </div>
    );
}

export default App;